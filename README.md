# PI


# edit raspian.img

- make mountdir `sudo mkdir /media/$USER/pi && sudo mkdir /media/$USER/pi_boot`
- open img file whit `sudo kpartx -a <raspian.img>`
- mount `sudo mount /dev/mapper/loop0p1 /media/$USER/pi_boot && sudo mount /dev/mapper/loop0p2 /media/$USER/pi`
- go to mountdir `cd /media/$USER/pi` or `cd /media/$USER/pi_boot`
- edit content as *root* and save (may edit `/etc/rc.local`, `wpa_supplicant.conf`, etc. as shown in next step)
- go out of mountdir `cd`
- `sync`
- umount `sudo umount /media/$USER/pi && sudo umount /media/$USER/pi_boot `
- close img file whit `sudo kpartx -d <raspian.img>`
- `sync`
- rm mountdir `sudo rm -r /media/$USER/pi && sudo rm -r /media/$USER/pi_boot`


## STARTUP

add the folowing to [`/media/$USER/pi`] `/etc/rc.local` to auto load and run the configfiles on startup:

```
sleep 5
wget 'https://gitlab.com/dog42/htwkpi/-/archive/master/htwkpi-master.zip' -O /home/pi/.master.zip
unzip /home/pi/.master.zip -d /home/pi/.piconf
rm /home/pi/.master.zip
./home/pi/.piconf/htwkpi-master/go.sh &>> /home/pi/.piconf/go.log
```

## WLAN

add `wpa_supplicant.conf` to boot-partition (`/media/$USER/pi_boot`)

```
country=DE
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
network={
       ssid="WLANNAME"
       psk="PASSWORD"
       key_mgmt=WPA-PSK
}
```
or for eduroam (also add `ca.pem` to `/home/pi/.cat_installer/ca.pem`)
```
country=DE
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
network={
        ssid="eduroam"
        key_mgmt=WPA-EAP
        pairwise=CCMP
        group=CCMP TKIP
        eap=PEAP
        ca_cert="/home/pi/.cat_installer/ca.pem"
        identity="NUTZER@htwk-leipzig.de"
        altsubject_match="DNS:radius.rz.htwk-leipzig.de"
        phase2="auth=MSCHAPV2"
        password="PASSWORD"
        anonymous_identity="eduroam@htwk-leipzig.de"
}
```

maybe the connection to eduroam doesn't work properly, in this case following steps may help:

1. edit `/lib/dhcpcd/dhcpcd-hooks/10-wpa_supplicant` and change `nl80211,wext` to `wext,nl80211` to prefer the wext driver

```
...
 wpa_cli -p "$dir" -i "$interface" status >/dev/null 2>&1 && return 0
        syslog info "starting wpa_supplicant"
        wpa_supplicant_driver="${wpa_supplicant_driver:-wext,nl80211}
        driver=${wpa_supplicant_driver:+-D}$wpa_supplicant_driver
        err=$(wpa_supplicant -B -c"$wpa_supplicant_conf" -i"$interface" \
            "$driver" 2>&1)
        errn=$?
...
```

2. run `sudo wpa_supplicant -D wext -i wlan0 -c /etc/wpa_supplicant/wpa_supplicant.conf` with explicit use of the wext driver

3. run `sudo wpa_supplicant -i wlan0 -c /etc/wpa_supplicant/wpa_supplicant.conf` without explicit use of the wext driver



## SSH

add an empty file named `ssh` (or `ssh.txt`) to boot-partition (`/media/$USER/pi_boot`) to activate SSH.

maybe also add your ssh-key [`/media/$USER/pi`] `/home/pi/.ssh/authorized_keys`

# write raspian.img

- run `lsblk -p` to see which devices are currently connected
- it will be listed as something like `/dev/mmcblk0` or `/dev/sdX` (eg. /dev/sdb)
- umount all partition of the SD card, for example `sudo umount /dev/sdb1` or `sudo umount /dev/mmcblk0p1`
- write the img `sudo dd bs=4M if=raspbian.img of=/dev/mmcblk0 conv=fsync status=progress && sync`
- run `sync`, this will ensure that the write cache is flushed and that it is safe to unmount your SD card.
