#!/bin/bash
echo "##############################################"
date
echo "##############################################"
ping -c 5 8.8.8.8

name="pi$(ip -o addr show dev "wlan0" | awk '$3 == "inet" {print $4}' | sed -r 's!/.*!!; s!.*\.!!')" && echo $name && hostname -b "$name" && echo $name > /etc/hostname && echo 127.0.0.1 $name >> /etc/hosts
apt-get update
#sudo apt-get -y upgrade
apt install -y git zsh htop i2c-tools libi2c-dev python-smbus
sed -i 's/nl80211,wext/wext,nl80211/g' /lib/dhcpcd/dhcpcd-hooks/10-wpa_supplicant # use wext driver
