#!/bin/bash                                                                                                                                                                                                     
if (( $(grep MemTotal /proc/meminfo | sed 's/[^0-9]*//g') > 750000 ));then
        bash -c 'echo -e "start_x=1 \ngpu_mem=512 " >> /boot/config.txt'
else
        bash -c 'echo -e "start_x=1 \ngpu_mem=128 " >> /boot/config.txt'
fi
apt install -y motion

cp /home/pi/.piconf/htwkpi-master/motion.conf /etc/motion/motion.conf
 
bash -c 'echo "start_motion_daemon=yes" > /etc/default/motion'
mkdir /home/pi/motion
chgrp motion /home/pi/motion
chmod g+rwx /home/pi/motion
(crontab -l 2>/dev/null; echo "0 0 * * * find /home/pi/motion -type f -mtime +30 -exec rm -f '{}' \;") | crontab -
 
#/etc/init.d/motion start
